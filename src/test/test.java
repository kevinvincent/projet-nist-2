package test;
import hl7ws.healthcare.nist.gov.validation.message.MessageValidationV2Interface;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;


public class test {

	public static void main(String[] args) {
		final MessageValidationV2Interface client;
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(MessageValidationV2Interface.class);
		factory.setAddress("http://hit-testing2.nist.gov:8090/hl7v2ws/services/soap/MessageValidationV2");
		client = (MessageValidationV2Interface) factory.create();
		System.out.println(client.getServiceStatus());
	}

}
