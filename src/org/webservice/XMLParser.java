package org.webservice;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class XMLParser {

	private final static String HEAD = "ns3:HL7V2MessageValidationReport";
	private final static String REPORT = "ns3:SpecificReport";
	private final static String ASSERTIONLIST = "ns3:AssertionList";
	private final static String ASSERTION = "ns3:Assertion";
	private final static String ATTRIBUTE_VALUE_ERROR = "error";
	private final static String ATTRIBUTE_RESULT = "Result";
	private final static String ATTRIBUTE_TYPE = "Type";
	private final static String LOCATION = "ns3:Location";
	private final static String LINE = "ns3:Line";
	private final static String COLUMN = "ns3:Column";
	private final static String PATH = "ns3:Path";
	private final static String DESCRIPTION = "ns3:Description";
	private String xml;
	
	public XMLParser(String path) {
		this.xml = path;
	}
	
	public ArrayList<Assertion> parse() {
		ArrayList<Assertion> resultArrayList = new ArrayList<>();
		try {
			parse(null, resultArrayList, null);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultArrayList;
	}
	
	public String displayErrors(ArrayList<Assertion> list) {
		String result = "There are " + list.size() + " errors." + "\n";
		int error = 0;
		for (Assertion assertion : list) {
			error++;
			result += "***** ERROR NUM " + error + " *****";
			result += assertion.toString();
			result +="\n\n";
		}
		return result;
	}

	private void parse(NodeList node, ArrayList<Assertion> assertList, Assertion assertion) throws SAXException, IOException, ParserConfigurationException {
		if (node == null) {
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        InputSource source = new InputSource(new StringReader(this.xml));
	        Document doc = db.parse(source);
	        doc.getDocumentElement().normalize();
	        NodeList nodeLst = doc.getElementsByTagName(XMLParser.HEAD);
	        Element element = (Element) nodeLst.item(0);
            NodeList fstNmElmntLst = element.getChildNodes();
            parse(fstNmElmntLst, assertList, assertion);
		} else {
			for (int i = 0; i<node.getLength(); i++) {
				Node nodeLoop = node.item(i);
				if (nodeLoop.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) nodeLoop;
					switch (nodeLoop.getNodeName()) {
						case XMLParser.REPORT:
							parse(element.getChildNodes(), assertList, assertion);
							break;
						case XMLParser.ASSERTIONLIST:
							parse(element.getChildNodes(), assertList, assertion);
							break;
						case XMLParser.ASSERTION:
							if (XMLParser.ATTRIBUTE_VALUE_ERROR
									.equals(element.getAttribute(XMLParser.ATTRIBUTE_RESULT))) {
								Assertion asser = new Assertion();
								asser.setType(element.getAttribute(XMLParser.ATTRIBUTE_TYPE));
								parse(element.getChildNodes(), assertList, asser);
							}
							break;
						case XMLParser.LOCATION :
							parse(element.getChildNodes(), assertList, assertion);
							break;
						case XMLParser.LINE :
							assertion.getLocation().setLine(nodeLoop.getTextContent());
							break;
						case XMLParser.COLUMN :
							assertion.getLocation().setColumn(nodeLoop.getTextContent());
							break;
						case XMLParser.PATH :
							assertion.getLocation().setPath(nodeLoop.getTextContent());
							break;
						case XMLParser.DESCRIPTION :
							assertion.setDescription(nodeLoop.getTextContent());
							assertList.add(assertion);
							break;
					}
		          }
			}
		}

	}
}
