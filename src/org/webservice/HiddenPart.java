package org.webservice;

import java.util.ArrayList;

import gov.nist.healthcare.hl7ws.client.MessageValidationV2SoapClient;

public class HiddenPart {
	
	public String validate(String hl7Message){
		String oid = "2.16.840.1.113883.3.72.2.3.99001";
		MessageValidationV2SoapClient client = new MessageValidationV2SoapClient("http://hit-testing2.nist.gov:8090/hl7v2ws/services/soap/MessageValidationV2");
		String report = client.validate(hl7Message, oid, null, null);
		XMLParser xml = new XMLParser(report);
		ArrayList<Assertion> assertion = xml.parse();
		return xml.displayErrors(assertion);
	}
}
