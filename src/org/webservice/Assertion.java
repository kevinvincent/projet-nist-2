package org.webservice;

public class Assertion {
	private String type;
	private String description;
	private Location location;
	
	public Assertion() {
		this.location = new Location();
		this.type = "";
		this.description = "";
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Location getLocation() {
		return location;
	}
	
	public String toString() {
		return "Type : " + this.type + "\n"
				+ this.location.toString() + "\n"
				+ "Description : " + this.description;
	}

	class Location {
		private String line;
		private String column;
		private String path;
		
		public String getLine() {
			return line;
		}
		public void setLine(String line) {
			this.line = line;
		}
		public String getColumn() {
			return column;
		}
		public void setColumn(String column) {
			this.column = column;
		}
		public String getPath() {
			return path;
		}
		public void setPath(String path) {
			this.path = path;
		}
		
		public String toString() {
			return "Line : " + this.line + "\n"
					+ "Column : " + this.column + "\n"
					+ "Path : " + this.path;
		}

	}
}
