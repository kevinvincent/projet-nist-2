package org.webservice;

import javax.jws.WebService;

@WebService
public class Message {

	public String submitSingleMessage (String Username, String password, String facilityID, String hl7Message) {
		HiddenPart hp = new HiddenPart();
		return hp.validate(hl7Message);
	}

	public String connectivityTest (String echoBack) {
		// Code
		return echoBack;
	}

}
